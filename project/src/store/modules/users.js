const usersStore = {
  state: {
    list:[
      {
        name: 'Denis',
        age: 10
      }
    ]
  },
  getters: {
    usersList: ({ list }) => {return list},
  },
  mutations: {
    ADD_USER(state, user) {
      state.list.push(user);
      console.log(state.list);
    }
  },
  actions: {
    newUser({ commit }, user) {
      const newUser = {...user, id: String(Math.random())};
      commit('ADD_USER', newUser);
    }
  },
  namespaced: true,
};

export default usersStore;