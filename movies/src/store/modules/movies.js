import indexArray from '../mock/imdb.js';
import mutations from '@/store/mutations.js';

function useData (movies) {
  return movies.reduce((acc, movie) => {
    for(var i =0; i< movies.length; i++) {
      acc[indexArray[i]] = movie;
    }
    return acc;
  }, {})
}

const { MOVIES } = mutations;

const axios = require('axios').default;
const moviesStore = {
  namespaced: true,
  state: {
    idsArray: indexArray,
    moviesPerPage: 10,
    currentPage: 1,
    movies: {},
  },
  getters: {
    moviesList ({ movies }) {
      return movies
    },
    getIds: ({ idsArray }) => (from, to) => idsArray.slice(from, to),
    currentPage: ({ currentPage }) => currentPage,
    moviesPerPage: ({ moviesPerPage }) => moviesPerPage,
  },
  mutations: {
    [MOVIES] (state, value) {
      state.movies = value;
    }
  },
  actions: {
    initMovies: {
      handler({ dispatch }) {
        dispatch('fetchMovies');
      },
      root: true,
    },
    async fetchMovies({ getters, commit }) {
      try {
        var { getIds, currentPage, moviesPerPage} = getters;
        var from = (currentPage * moviesPerPage) - moviesPerPage;
        var to = currentPage * moviesPerPage;
        var moviesFetch = getIds(from, to);
        //process.env.VUE_APP_API_KEY
        const requests = moviesFetch.map(id => axios.get(`/?i=${id}`));
        const response = await Promise.all(requests);
        var movies = useData(response);
        commit(MOVIES, movies);
      } catch (err) {
        console.log(err);
      }
    }
  },
};

export default moviesStore;