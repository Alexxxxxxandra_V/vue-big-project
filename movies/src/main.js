import { createApp } from 'vue';
import App from './App.vue';
import store from './store';
//import './plugins/bootstrap/import.js';
import './plugins/axios/import.js';

createApp(App).use(store).mount('#app')
