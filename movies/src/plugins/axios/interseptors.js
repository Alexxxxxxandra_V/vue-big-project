function setParams(config) {
  console.log(config);
  const params = config.params || {};
  config.params = Object.assign(params, {
    apikey: process.env.VUE_APP_API_KEY,
    plot: 'full',
  })
}

export default function (axios) {
  axios.interсeptors.request.use(setParams);
}